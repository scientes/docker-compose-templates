version: '3.7'

services:

  traefik:
    # Use the latest Traefik image
    image: traefik:latest
    ports:
      - target: 80
        published: 80
        protocol: tcp
        mode: host
      - target: 443
        published: 443
        protocol: tcp
        mode: host
    deploy:
      placement:
        constraints:
          # Make the traefik service run only on the node with this label
          # as the node with it has the volume for the certificates
          - node.labels.traefik-public.traefik-public-certificates == true
      labels:
        # Enable Traefik for this service, to make it available in the public network
        - traefik.enable=true
        # Use the traefik-public network (declared below)
        - traefik.docker.network=traefik-public
        # Use the custom label "traefik.constraint-label=traefik-public"
        # This public Traefik will only use services with this label
        # That way you can add other internal Traefik instances per stack if needed
        - traefik.constraint-label=traefik-public
        # admin-auth middleware with HTTP Basic auth
        # Using the environment variables USERNAME and HASHED_PASSWORD
        - traefik.http.middlewares.admin-auth.basicauth.users=${USERNAME?Variable not set}:${HASHED_PASSWORD?Variable not set}
        # set domain
        - traefik.http.routers.traefik.rule=Host(${TRAEFIC_DOMAIN})
        # traefik-http set up only to use the middleware to redirect to https
        - traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)
        - traefik.http.routers.http-catchall.entrypoints=web
        - traefik.http.routers.http-catchall.middlewares=redirect-to-https
        # define redirect scheme
        - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
        - traefik.http.middlewares.redirect-to-https.redirectscheme.permanent=true

        # Use the special Traefik service api@internal with the web UI/Dashboard
        - traefik.http.routers.traefik.entrypoints=websecure
        - traefik.http.routers.traefik.service=api@internal
        # Use the "le" (Let's Encrypt) resolver created below
        - traefik.http.routers.traefik.tls.certresolver=le
        # Enable HTTP Basic auth, using the middleware created above
        - traefik.http.routers.traefik.middlewares=admin-auth
        # Define the port inside of the Docker service to use
        - traefik.http.services.traefik-public.loadbalancer.server.port=8080
    volumes:
      # Add Docker as a mounted volume, so that Traefik can read the labels of other services
      - /var/run/docker.sock:/var/run/docker.sock:ro
      # Mount the volume to store the certificates
      - traefik-public-certificates:/certificates
    command:
      # Enable Docker in Traefik, so that it reads labels from Docker services
      - --providers.docker
      # Add a constraint to only use services with the label "traefik.constraint-label=traefik-public"
      - --providers.docker.constraints=Label(`traefik.constraint-label`, `traefik-public`)
      # Do not expose all Docker services, only the ones explicitly exposed
      - --providers.docker.exposedbydefault=false
      # Enable Docker Swarm mode
      - --providers.docker.swarmmode
      # Create an entrypoint "http" listening on address 80
      - --entrypoints.web.address=:80
      # Create an entrypoint "https" listening on address 443
      - --entrypoints.websecure.address=:443
      # Create the certificate resolver "le" for Let's Encrypt, uses the environment variable EMAIL
      - --certificatesresolvers.le.acme.email=${EMAIL?Variable not set}
      # Store the Let's Encrypt certificates in the mounted volume
      - --certificatesresolvers.le.acme.storage=/certificates/acme.json
      # Use the TLS Challenge for Let's Encrypt
      # - --certificatesresolvers.le.acme.tlschallenge=true
      - --certificatesresolvers.le.acme.dnschallenge=true
      # Use Provider this time cloudflare
      - --certificatesresolvers.le.acme.dnschallenge.provider=cloudflare
      # Enable the access log, with HTTP requests
      - --accesslog
      # - --log.level=DEBUG #
      # Enable the Traefik log, for configurations and errors
      - --log
      # Enable the Dashboard and API
      - --api
      - --entryPoints.web.forwardedHeaders.insecure
      # enable metric for monitoring
      - --metrics
      - --metrics.prometheus.buckets=0.1,0.3,1.2,5.0
    environment:
      - CF_DNS_API_TOKEN=${CF_TOKEN}
    networks:
      # Use the public network created to be shared between Traefik and
      # any other service that needs to be publicly available with HTTPS
      - traefik-public
  agent:
    image: portainer/agent:2.13.1
    environment:
      AGENT_CLUSTER_ADDR: tasks.agent
      LOG_LEVEL: debug
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    networks:
      - agent-network
    deploy:
      mode: global
      placement:
        constraints:
          - node.platform.os == linux
  portainer:
    image: portainer/portainer-ce:2.13.1
    command: -H tcp://tasks.agent:9001 --tlsskipverify
    volumes:
      - portainer-data:/data
    networks:
      - agent-network
      - traefik-public
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
          - node.labels.portainer.portainer-data == true
      labels:
        # Frontend
        - traefik.enable=true
        - traefik.http.routers.frontend.rule=Host(${DOMAIN_PORTAINER})
        - traefik.http.routers.frontend.entrypoints=websecure
        - traefik.http.services.frontend.loadbalancer.server.port=9000
        - traefik.http.routers.frontend.service=frontend
        - traefik.http.routers.frontend.tls.certresolver=le

        # Edge
        - traefik.http.routers.edge.rule=Host(${DOMAIN_EDGE_PORTAINER})
        - traefik.http.routers.edge.entrypoints=websecure
        - traefik.http.services.edge.loadbalancer.server.port=8000
        - traefik.http.routers.edge.service=edge
        - traefik.http.routers.edge.tls.certresolver=le

        # load network
        - traefik.docker.network=traefik-public
        - traefik.constraint-label=traefik-public

volumes:
  traefik-public-certificates:
  portainer-data:

networks:
  # Use the previously created public network "traefik-public", shared with other
  # services that need to be publicly available via this Traefik
  traefik-public:
    external: true
  agent-network:
    attachable: true