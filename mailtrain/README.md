envvars:

DOMAIN: domain for mailtrain trusted server
DOMAIN_TRUSTED: domain for mailtrain sandbox server
DOMAIN_PUBLIC: domain for mailtrain public server