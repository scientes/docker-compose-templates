## setup behind a proxy

add trusted proxys in the config file. under /etc/zulip/zulip.conf
`
[loadbalancer]
ips = 10.0.0.0/24
`

## create after wards if the container crashes an new organsistaion
under /home/zulip/deployments/current
./manage.py generate_realm_creation_link 

# setup oidc 

Enable zproject.backends.GenericOpenIdConnectBackend in the AUTHENTICATION_BACKENDS setting in /etc/zulip/settings.py.
or set GenericOpenIdConnectBackend in env var